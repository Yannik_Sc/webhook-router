use std::fmt::{Debug, Formatter};
use std::net::SocketAddr;
use std::{collections::HashMap, path::PathBuf};

use axum::routing::get;

mod routes;

#[derive(Clone)]
pub struct AppState {
    config: Config,
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
pub struct Converter {
    path: PathBuf,
    function: String,
    #[serde(default)]
    settings: HashMap<String, serde_json::Value>,
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
pub struct Target {
    url: String,
    #[serde(default = "default_method")]
    method: String,
}

fn default_method() -> String {
    "GET".to_string()
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
pub struct Config {
    listen_address: SocketAddr,
    converters: HashMap<String, Converter>,
    targets: HashMap<String, Target>,
}

pub struct LogError(String);

impl<E: std::error::Error> From<E> for LogError {
    fn from(value: E) -> Self {
        log::error!("Could not start: {value}");

        LogError(std::any::type_name::<E>().to_string())
    }
}

impl Debug for LogError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.0)
    }
}

#[tokio::main]
pub async fn main() -> Result<(), LogError> {
    env_logger::init();
    let config_path = std::env::current_dir()?.join("webhook-router.toml");
    let state = AppState {
        config: toml::from_str(&std::fs::read_to_string(&config_path)?)?,
    };

    log::info!("Starting server on {}", state.config.listen_address);
    let server =
        axum::Server::from_tcp(std::net::TcpListener::bind(&state.config.listen_address)?)?;
    let router = axum::Router::default()
        .route(
            "/incoming/:converter/:target",
            get(routes::handle_webhook).post(routes::handle_webhook),
        )
        .route(
            "/echo",
            get(routes::print_echo_body).post(routes::print_echo_body),
        )
        .layer(tower_http::trace::TraceLayer::new_for_http())
        .with_state(state);

    server.serve(router.into_make_service()).await?;

    Ok(())
}
