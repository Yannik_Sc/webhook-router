# Webhook Router

The webhook router is supposed to catch webhooks, transform the request with the help of lua scripts and push them to
the next service, where they are supposed to go, returning the resulting response.

## Config

The config file has to be named `webhook-router.toml` and has to be in the current working directory.

- `converters` - Contains the available "converters" (aka lua scripts, that modify the request)
    - `[name:string]`
        - `path` - `[string]` The path to the script relative to the current working directory
        - `function` - `[string]` The function inside the script, that should be called for the conversion
        - `settings` - `[map<string, _>]` Possible user data, available in the script
- `targets`
    - `[name:string]`
        - `url` - `[string]` The url where the modified request gets forwarded to

### Global Lua variables

- `settings` - Contains user data specified for the converter in the config
- `request` - Contains information regarding the request
    - `headers` - `[map<string, string[]>]` All the request headers
    - `uri` - `[string]` The full request uri
    - `method` - `[string]` The received method (GET and POST are currently supported)
    - `query` - `[map<string, string>]` The query parameters as map

### Converter function return

- `body` - Any valid JSON value, which will be sent to the remote
- `headers` - `[(string, string)[]]` An array of string tuples containing header name and value
