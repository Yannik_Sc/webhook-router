use std::{collections::HashMap, str::FromStr};

use axum::{
    extract::{Json, Path, Query, State},
    http::{HeaderMap, Method, StatusCode, Uri},
    response::{IntoResponse, Response},
};
use mlua::LuaSerdeExt;

use crate::AppState;

pub struct OpaqueServerError(StatusCode);

impl<E: std::error::Error> From<E> for OpaqueServerError {
    fn from(value: E) -> Self {
        log::error!("Unable to execute: {value:#?}");

        Self(StatusCode::INTERNAL_SERVER_ERROR)
    }
}

#[axum::async_trait]
impl IntoResponse for OpaqueServerError {
    fn into_response(self) -> Response {
        self.0.into_response()
    }
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
pub struct LuaResponse {
    #[serde(default)]
    headers: Vec<(String, String)>,
    #[serde(default)]
    body: serde_json::Value,
}

pub async fn print_echo_body(body: Json<serde_json::Value>) -> impl IntoResponse {
    log::debug!("{body:#?}");

    body
}

pub async fn handle_webhook(
    Path((converter, target)): Path<(String, String)>,
    headers: HeaderMap,
    uri: Uri,
    State(app): State<AppState>,
    method: Method,
    Query(query): Query<serde_json::Value>,
    body: Option<Json<serde_json::Value>>,
) -> Result<impl IntoResponse, OpaqueServerError> {
    let lua_response: LuaResponse = {
        let lua = mlua::Lua::new();
        let request = lua.create_table()?;
        let headers: Result<HashMap<_, Vec<_>>, OpaqueServerError> =
            headers
                .iter()
                .fold(Ok(HashMap::new()), |mut aggregator, (name, value)| {
                    let name = name.to_string();
                    let value = value.to_str()?.to_string();

                    if let Ok(aggregator) = &mut aggregator {
                        aggregator.entry(name).or_insert(Vec::new()).push(value);
                    }

                    aggregator
                });
        request.set("headers", headers?)?;
        request.set("uri", uri.to_string())?;
        request.set("method", method.to_string())?;
        request.set("query", lua.to_value(&query)?)?;

        let converter = app.config.converters.get(&converter).ok_or_else(|| {
            log::error!("No converter with name {converter} found!");
            OpaqueServerError(StatusCode::INTERNAL_SERVER_ERROR)
        })?;
        let script = std::fs::read_to_string(std::env::current_dir()?.join(&converter.path))?;
        let converter_settings = converter
            .settings
            .iter()
            .map(|(key, value)| Ok((key.clone(), lua.to_value(value)?)))
            .collect::<Result<HashMap<_, _>, OpaqueServerError>>()?;

        lua.load(&script).exec()?;
        lua.globals().set("request", request)?;
        lua.globals().set("settings", converter_settings)?;

        let body_value = lua.to_value(&body.map(|extractor| extractor.0));
        let lua_function: mlua::Function = lua.globals().get(converter.function.clone())?;
        let lua_response: mlua::Value = lua_function.call(body_value)?;
        serde_json::from_value(serde_json::to_value(&lua_response)?)?
    };

    let target = app.config.targets.get(&target).ok_or_else(|| {
        log::error!("No target with name {target} found!");
        OpaqueServerError(StatusCode::INTERNAL_SERVER_ERROR)
    })?;
    let client = reqwest::ClientBuilder::default().build().unwrap();
    let mut request = client.request(Method::from_str(&target.method)?, &target.url);

    if !lua_response.body.is_null() {
        request = request.json(&lua_response.body);
    } else {
        request = request.json(&serde_json::Value::Object(Default::default()));
    }

    for (name, value) in lua_response.headers {
        request = request.header(name, value);
    }

    let request = request.build()?;
    let response = client.execute(request).await?;

    let headers = response.headers().clone();
    let status = response.status();
    let version = response.version();

    let mut axum_response = Response::new(response.text().await?);
    *axum_response.headers_mut() = headers;
    *axum_response.status_mut() = status;
    *axum_response.version_mut() = version;

    Ok(axum_response)
}
