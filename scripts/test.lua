function test(request_body)
  print("Test")
  print(request.uri)

  if settings then
   print(settings.print_this_to_console)
  end

  for k, v in pairs(request.headers) do
    print(k)

    for _, value in ipairs(v) do
        print("  ", value)
    end
  end

  for k, v in pairs(request.query) do
    print(k, v)
  end

  for k, v in pairs(settings) do
    print(k, v)
  end

  if request.method == "GET" then
    print("Got GET method")
  end

  if request.method == "POST" then
    for k, v in pairs(request_body) do
      print(k, v)
    end
  end

  return {
    headers = {{'Content-Type', 'application/json'}, {'custom-header', 'my custom header'}},
    body = request_body,
  }
end
